import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class mainUI {
    private JButton standardButton;
    private JButton greenonionButton;
    private JButton gratedJapaneseRadishButton;
    private JButton cheeseButton;
    private JButton kimuchiButton;
    private JButton gratedYamButton;
    private JTextPane orderedItemsList;
    private JLabel totalPay;
    private JButton checkOutButton;
    private JPanel main;
    private JButton cancelButton;


    int std = 400;
    int green = 550;
    int gjr = 560;
    int chee = 570;
    int kim = 610;
    int gry = 620;
    int total = 0;
    Timer timer = new Timer();

    public static int Value = 0;
    public static int number = 0;
    public static int check = 0;
    int temp = 0;
    public static int orderMenu = 0;
    public static int many = 1;
    public static int size = 0;

    sizeUI sizeUI;


    public mainUI() {

        standardButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sizeUI = new sizeUI(std);
                sizeUI.setVisible(true, std);
            }
        });

        standardButton.setIcon(new ImageIcon(this.getClass().getResource("gyu_hp_s.png")));
        greenonionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sizeUI = new sizeUI(green);
                sizeUI.setVisible(true, green);


            }
        });
        greenonionButton.setIcon(new ImageIcon(this.getClass().getResource("gyu_negi_negi_hp_s.png")));
        gratedJapaneseRadishButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sizeUI = new sizeUI(gjr);
                sizeUI.setVisible(true, gjr);


            }
        });
        gratedJapaneseRadishButton.setIcon(new ImageIcon(this.getClass().getResource("gyu_oroshi_hp_s.png")));
        cheeseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sizeUI = new sizeUI(chee);
                sizeUI.setVisible(true, chee);


            }
        });
        cheeseButton.setIcon(new ImageIcon(this.getClass().getResource("gyu_cheese_hp_s_230411.png")));
        kimuchiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sizeUI = new sizeUI(kim);
                sizeUI.setVisible(true, kim);


            }
        });
        kimuchiButton.setIcon(new ImageIcon(this.getClass().getResource("gyu_negitama_hp_s.png")));
        gratedYamButton.setIcon(new ImageIcon((this.getClass().getResource("gyu_negi_tororo_hp_s_230411.png"))));
        gratedYamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sizeUI = new sizeUI(gry);
                sizeUI.setVisible(true, gry);


            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(null, "Would you like to check out?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
                if (result == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for your order! \n The total price is " + total + " JPY");
                    orderedItemsList.setText("");
                    total = 0;
                    Value = 0;
                    totalPay.setText("Total: " + total + " JPY");
                }

            }
        });

//商品ボタンが押されたとき、sizeUI側でのValueの値を受け取る
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
//                sizeUIがnullでないとき、すなわち商品ボタンが押されたとき
                if (Objects.nonNull(sizeUI)) {

                    if (check==0) {

//                        System.out.println(total);//debug

                    } else if(check==1){
//                        checkの値が1のとき、sizeUI側でのValueの値を受け取り、sizeUIをnullにする
//                        リストや、合計金額の更新もここで行う
                        total += Value;
                        String currentText = orderedItemsList.getText();

                        switch (orderMenu) {
                            case 400:
                                currentText += "Standard";
                                break;
                            case 550:
                                currentText += "Green Onion";
                                break;
                            case 560:
                                currentText += "Grated Japanese Radish";
                                break;
                            case 570:
                                currentText += "Cheese";
                                break;
                            case 610:
                                currentText += "Kimuchi";
                                break;
                            case 620:
                                currentText += "Grated Yam";
                                break;
                        }
                        switch (size) {
                            case -50:
                                currentText += " :S";
                                break;
                            case 0:
                                currentText += " :M";
                                break;
                            case 100:
                                currentText += " :L";
                                break;
                        }
                        switch (many) {
                            case 1:
                                currentText += " :1 :"+Value+"JPY" + "\n";
                                break;
                            case 2:
                                currentText += " :2 :"+Value+"JPY" + "\n";
                                break;
                            case 3:
                                currentText += " :3 :"+Value+"JPY" + "\n";
                                break;

                        }
                        orderedItemsList.setText(currentText);

                        totalPay.setText("Total: " + total + " JPY");
                        sizeUI = null;
                        check=0;
                        temp++;
                    }else if(check==2){
//                        checkが2のときsizeUI側でキャンセルされたのでsuzeUIをnullにする処理のみを行いrunを停止
                        sizeUI = null;
                        check=0;
                    }


                }

            }
        }, 500, 500);
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(null, "Would you like to cancel the order?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
                if (result == 0) {
                    orderedItemsList.setText("");
                    total = 0;
                    Value = 0;
                    totalPay.setText("Total: " + total + " JPY");
                }
            }
        });
    }

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        JFrame frame = new JFrame("mainUI");
        frame.setContentPane(new mainUI().main);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


    }
}



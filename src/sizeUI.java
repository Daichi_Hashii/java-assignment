import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;


public class sizeUI extends mainUI {
    private JButton cancelButton;
    private JButton OKButton;
    public JPanel sizeUI;
    public JRadioButton sizeButton3;
    public JRadioButton sizeButton2;
    public JRadioButton sizeButton1;

    public JRadioButton manyButton1;
    public JRadioButton manyButton2;
    public JRadioButton manyButton3;
    private JLabel notify;
    private JLabel Howm;
    private JLabel Hows;
    private JLabel Price;






    public void setTotal(int t) {
        mainUI.Value = t;
    }

    public int getTotal() {
        return mainUI.Value;
    }


    public int ordertotal(int menu, int check) {
        int t = (size * many) + (menu * many);
        check = 0;
        setTotal(t);
        return t;
    }

    private void MessageWindow(String m) {
        notify.setText("Please close this window.");
        JOptionPane.showMessageDialog(null, "Thank you for ordering " + m + "!\n It will be served as soon as possible.");
    }


    public sizeUI() {

    }

    public sizeUI(int menu) {

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result = JOptionPane.showConfirmDialog(null, "Would you like to cancel your order?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
                if (result == 0) {
                    check = 2;
                    cancelButton.setVisible(false);
                    OKButton.setVisible(false);
                    sizeButton1.setVisible(false);
                    sizeButton2.setVisible(false);
                    sizeButton3.setVisible(false);
                    manyButton1.setVisible(false);
                    manyButton2.setVisible(false);
                    manyButton3.setVisible(false);
                    Howm.setVisible(false);
                    Hows.setVisible(false);
                    Price.setVisible(false);
                    notify.setText("Please close this window.");
                }
            }
        });
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (size < -400 || many < -10) {
                    JOptionPane.showMessageDialog(null, "Please select the size and quantity.", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                } else {
//                    int result = JOptionPane.showConfirmDialog(null, "Is this correct?", "Order Confirmation", JOptionPane.YES_NO_OPTION);
//
//                    if (result == 0) {


                    check = 1;
                    Value = ordertotal(menu, check);
                    orderMenu = menu;
                    number++;
//                    System.out.println("ssssssss" + total);
                    cancelButton.setVisible(false);
                    OKButton.setVisible(false);
                    sizeButton1.setVisible(false);
                    sizeButton2.setVisible(false);
                    sizeButton3.setVisible(false);
                    manyButton1.setVisible(false);
                    manyButton2.setVisible(false);
                    manyButton3.setVisible(false);
                    Howm.setVisible(false);
                    Hows.setVisible(false);
                    Price.setVisible(false);
                    switch (orderMenu) {
                        case 400:
                            MessageWindow("Standard");
                            break;
                        case 550:
                            MessageWindow("Green Onion");
                            break;
                        case 560:
                            MessageWindow("Grated Japanese Radish");
                            break;
                        case 570:
                            MessageWindow("Cheese");
                            break;
                        case 610:
                            MessageWindow("Kimuchi");
                            break;
                        case 620:
                            MessageWindow("Grated Yam");
                            break;
                    }

//                    }
                }

            }
        });


        ActionListener listener = new ActionListener() {


            @Override
            public void actionPerformed(ActionEvent e) {
                ButtonGroup sizeButtonGroup = new ButtonGroup();
                sizeButtonGroup.add(sizeButton1);
                sizeButtonGroup.add(sizeButton2);
                sizeButtonGroup.add(sizeButton3);
                if (sizeButton1.isSelected()) {
                    size = -50;
                } else if (sizeButton2.isSelected()) {
                    size = 0;
                } else if (sizeButton3.isSelected()) {
                    size = 100;

                } else {
                    size = -500;
                }
            }
        };
        sizeButton3.addActionListener(listener);
        sizeButton2.addActionListener(listener);
        sizeButton1.addActionListener(listener);

        ActionListener mlistener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ButtonGroup manyButtonGroup = new ButtonGroup();
                manyButtonGroup.add(manyButton1);
                manyButtonGroup.add(manyButton2);
                manyButtonGroup.add(manyButton3);
                if (manyButton1.isSelected()) {
                    many = 1;
                } else if (manyButton2.isSelected()) {
                    many = 2;
                } else if (manyButton3.isSelected()) {
                    many = 3;
                } else {
                    many = -20;
                }
            }
        };
        manyButton3.addActionListener(mlistener);
        manyButton2.addActionListener(mlistener);
        manyButton1.addActionListener(mlistener);
    }

    public static void main(String[] args) throws UnsupportedLookAndFeelException {
        JFrame frame = new JFrame("sizeUI");
        frame.setContentPane(new sizeUI().sizeUI);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    public void setVisible(boolean b, int menu) {
        JFrame frame = new JFrame("sizeUI");
        frame.setContentPane(new sizeUI(menu).sizeUI);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        if (b == false) {
            frame.removeAll();
        }
    }
}
